<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- Output are dependent of user's authentication -->

<sec:authorize access="isAuthenticated()">
  Hello <sec:authentication property="principal.username" />! 
  &nbsp;
  <a href="<c:url value='/editaccount?un=${pageContext.request.userPrincipal.name}' />">Edit Account &nbsp;</a>
  <a href="<c:url value="/j_spring_security_logout" />">Logout</a> &nbsp;
</sec:authorize>

<sec:authorize access="!isAuthenticated()">
  <a href="<c:url value="/login" />">Login</a>&nbsp;    
</sec:authorize>