<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



<form action="result" method="POST" enctype="multipart/form-data" >
    <div class="form-group">
        <label>File input</label>
        <input type="file" name="file"/>
    </div>
    <br/><br/>
    <div class="form-group">
        <button type="submit" id="upload">Upload</button>
    </div>
</form>