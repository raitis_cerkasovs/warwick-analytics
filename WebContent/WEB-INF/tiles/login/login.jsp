<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="help">
<p>Log in to the account.</p>
</div>

<!-- Spring MVC built in authentication activity -->
<form name='f' action='${pageContext.request.contextPath}/j_spring_security_check' method='POST'> 
 <table>
 
     <tr>
      <td></td>
      <td>
        <c:if test="${param.error != null}">
            <span class="error">Wrong credentials</span>
        </c:if>
      </td>
    </tr>

    <tr>
      <td>User:</td>
      <td><input type='text' name='j_username' value=''></td>
    </tr>
    
    <tr>
      <td>Password:</td>
      <td><input type='password' name='j_password'/></td>
    </tr>  
      
    <tr>
      <td>Remember Me:</td>
      <td align="left"><input type='checkbox' name='_spring_security_remember_me' checked="checked"></td>
    </tr>
    
    <tr>
      <td colspan='2'><input class="submit" name="submit" type="submit" value="Login"/></td>
    </tr>
  </table>
</form>

<div class="mainMenu"><a style="text-align: left;" href="<c:url value="/" />">Home</a></div>

</body>
</html>