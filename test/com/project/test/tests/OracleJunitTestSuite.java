package com.project.test.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author raitis
 * 
 * Test suite for all DAO tests
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
   OracleTests.class
})


public class OracleJunitTestSuite {   
}