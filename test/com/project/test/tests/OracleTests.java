package com.project.test.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.dao.OracleTest;
import com.project.dao.OracleTestDAO;
import com.project.dao.User;


@ActiveProfiles("development")
@ContextConfiguration(locations = {
		"classpath:com/project/config/dao-context.xml", 
		"classpath:com/project/config/security-context.xml",
		"classpath:com/project/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class OracleTests {
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private OracleTestDAO oracleTestDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	
	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from OracleTest");

	}
		
	@Test
	public void testSaveOrUpdateOracle() {
		
		OracleTest oracleTest = new OracleTest();
		oracleTest.setId("1");
		oracleTest.setName("ok");
		oracleTestDAO.saveOrUpdate(oracleTest);
		
	    List<OracleTest> OracleTestList = oracleTestDAO.getAllOracleTestRecords();
	    assertEquals("One record should be retrieved", 1, OracleTestList.size());	
		
	}
	
}