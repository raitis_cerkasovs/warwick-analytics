It is obvious that this is a small project. Appropriately would be to use, for example, the php script. It would take a few hours to create a quality code with validators, easy to deploy.
However, it was mentioned as a big project, so for initial settings I went big.
I use:
Spring MVC framework
Oracle database
Maven, to organize libraries
Spring security
Hibernate as ORM
JUnit for tests
Apache Tailes to organize interface
OpenCSC to read CSV files.

Obviously it wouldn’t be possible to make all configurations within two hours. I use frame, which was preconfigured for my previous projects (it is not automatically generated but configured by hand).
To upload CSV files there is no need for databases, however, as it was my task to initialize project I left it in. For example, to store reference to uploaded files etc.
The project use two oracle databases “wa” as a main and “watest” for JUnit tests. 
I left classes responsible for authorization. 
The main task (upload and edit CSV files) unfortunately are not finished due lack of time. It employs “HmeController” and “home.jsp” and “result.jsp” tails.
There is amended second condition for removing records, and it is reading only third variable for conditions.
To get a frame ready, generate two databases along with the user privileges took about ½ h, to code file upload and partly data manipulation – more than an hour. The rest was deploying and report.

Resources

Program is deployed on my AWS EC2 Linux instance, uses Apache Tomcat webserver.
http://raitis.co.uk:8080/WarwickAnalyticsTest/
Oracle database is AWS RDS instance
Main database:
Username: we
Password: we
Schema: we
SID: bizdb
Host: bizdb.csjbpckh1ndk.eu-west-1.rds.amazonaws.com
Port: 1521

Test database:
Username: wetest
Password: wetest
Schema: wetest
Code is deployed to Bitbacket repository:
https://bitbucket.org/raitis_cerkasovs/warwick-analytics/src
It has all files (including dependencies) so there should be no problem to run it.

Frame structure

The back-end consists of two main folders - test and src.

Test: 

Used for JUnit tests.
Com.project.test.tests – for actual tests. Currently have only one test for connectivity to database
Com.project.test.config – hold configuration relative to access to test database

Src:

Com,project.config:
dao-context: dtabases and Hibernate configuration
Jdbc-oracle-properties: database credentials
Security-context: Spring security. If a new page is created it has to be registered here.
Service-context: configuration for services.

Com.project.controliers: MVC controllers. HomeController is used for CSV task, LoginControlier is left for authentication.

Ccom.project.dao: Data access object. Classes for each table.

Com.project.messages: text for validators or different languages.

Com.project.servises: Services package. It is good practice separate for example data access from dao.

Com.project.validation: validators.

Front-end:

Dispatch-servlet: first configuration instance.

Flows – folder for webflows.

Layouts: has a file to register tails.

Templates: page layout.

Tails: keep all registered jsp files.


Main task

To work with CSV files I use additional java library open.csv-3.4. 
To establish file upload - commons-fileupload-1.3.1.
The code example for open.csv comes from here: 
http://opencsv.sourceforge.net
By using file upload I had a problem with Multipart Resolver configuration.
This resource helped for me to resolve it:
http://learningviacode.blogspot.co.uk/2012/11/upload-and-download-files.html
And for main and most interesting part – data manipulation, unfortunately I didn’t have time left ☺
Here is deployed sample:
http://raitis.co.uk:8080/WarwickAnalyticsTest/
CSV file I use for tests is within an email attachement.
