package com.project.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;


/**
 * @author raitis
 * 
 * Controller for initial pages - home, information, "My Jobs" for courier profile
 */ 
@Controller
public class HomeController {
	
	/**
	 * @return
	 * 
	 * First initial page controller
	 */
	@RequestMapping("/")
	 public String showHome() {
	
		return "home";
		 
	 }	
	
	/**
	 * @return
	 * 
	 * Return view for "result" page
	 */
	@RequestMapping(value="/result", method = RequestMethod.POST)
	public String uploadFile(
	        ModelMap model,
	        @RequestParam MultipartFile file,
	        HttpServletRequest request) {
		
		// empty file validation
		if (file.isEmpty()) {
		   model.put("msg", "failed to upload file because its empty");
		   return "home";
		}
		

	    String rootPath = request.getSession().getServletContext().getRealPath("/");
	    File dir = new File(rootPath + File.separator + "uploadedfile");
	    if (!dir.exists()) {
	        dir.mkdirs();
	    }
	 
	    File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
	 
	    try {
	        try (InputStream is = file.getInputStream();
	                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile))) {
	            int i;
	            //write file to server
	            while ((i = is.read()) != -1) {
	                stream.write(i);
	            }
	            stream.flush();
	        }
	    } catch (IOException e) {
	        model.put("msg", "failed to process file because : " + e.getMessage());
	        return "home";
	    }
	 
	   String[] nextLine;
	   String[] records;
	   List<String> result = new ArrayList<String>();
	    try {

	        try (FileReader fileReader = new FileReader(serverFile);
	            CSVReader reader = new CSVReader(fileReader, ';', '\'', 1);){
	        	
	        	
       /**
        * CSV file data manipulation
        * 
        * Not finished due 2h limit. 
        * The resulting list must be passed as a list of lists. Currently it is passed as one value.
        * The result is striping only lines where 3rd value is 0, must be added loop for possible arbitrary amount of vars.
        * Second condition is amended.
        * All types of validation is amended.
        */	        	
///////////////////////////////////////////////////////////////////////////	                    	
	        	
	        	while ((nextLine = reader.readNext()) != null) {

	                records = nextLine[0].split(",");
	                
	                if (!records[2].equals("0")) {
	                	   // test output
	                       System.out.println(nextLine[0]);
	                       result.add(nextLine[0]);
	                }
	            }
            
	            
//////////////////////////////////////////////////////////////////////////////	            
	        }
	    } catch (IOException e) {
	        System.out.println("error while reading csv : " + e.getMessage());
	    } 
	 
	    
        model.addAttribute("result", result); 
	    return "result";
	}	

}
