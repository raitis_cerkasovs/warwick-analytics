package com.project.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.dao.User;
import com.project.service.UsersService;

/**
 * @author raitis
 *
 * Functions related to user account
 */
@Controller
public class LoginController {

	/**
	 * Initialize service
	 */
	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}
	
	
	/**
	 * @param model - pass "user" parameter to view
	 * @param un - username
	 * @return
	 * 
	 * Function to open form for user data editing
	 */
	@RequestMapping(value = "/editaccount")
	public String editAccount(Model model, @RequestParam("un") String un) {
		
		User user = usersService.getUser(un);
		model.addAttribute("user", user);

		return "editaccount";
	}
	
	
	/**
	 * @param model - not used
	 * @param user - user object
	 * @param result - validator's error list
	 * @return
	 * 
	 * Function to save updated user
	 */
	@RequestMapping(value = "/updateaccount")
	public String updateAccount(Model model, @Valid User user, BindingResult result) {
		
		if (result.hasErrors()) {
			return "editaccount";
		} 			
		usersService.saveOrUpdateUser(user);
		
		return "home";
	}
	
	
	/**
	 * @return
	 * 
	 * Login function (integrated from SpringMVC)
	 */
	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	
	/**
	 * @return
	 * Logout function (integrated from SpringMVC)
	 */
	@RequestMapping("/logout")
	public String showLogout() {
		return "logout";
	}


	/**
	 * @return
	 * 
	 * Function to return "accessdenied view"
	 */
	@RequestMapping("/accessdenied")
	public String showAccessDenied() {
		return "accessdenied";
	}



}
