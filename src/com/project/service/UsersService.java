package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.Authorities;
import com.project.dao.User;
import com.project.dao.UsersDAO;

/**
 * @author raitis
 * 
 * Wrapper class for DAO user, courier and client data update functions
 */
@Service("usersService")
public class UsersService {
	
	@Autowired
	private UsersDAO usersDao;

	/**
	 * @param user - user object from form
	 * @param role - authority parameter
	 * 
	 * Function to save new user. Format user fields before saving
	 */
	public void saveUser(User user, String role) {
        	
        Authorities authorities = new Authorities();	
		authorities.setAuthority(role);
     	authorities.setUsername(user.getUsername());
     	user.setAuthorities(authorities);
		user.setEnabled(true);
		
		usersDao.save(user);		
	}
	
	
	public void saveOrUpdateUser(User user) {    	
		usersDao.saveOrUpdate(user);		
	}
	
    public User getUser(String username) {
    	return usersDao.getUser(username);
    }
    
	public List<User> getUsers(String role) {
        return usersDao.getUsers(role);
	}	


}
