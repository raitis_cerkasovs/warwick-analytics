package com.project.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.project.validation.ValidEmail;


/**
 * @author raitis
 *
 * Class User, hold a row of users table
 */
@Entity
@Table(name="users")
public class User implements Serializable {
	
	private static final long serialVersionUID = 3952198401362088846L;

	@Id
	@Column(name="username", unique=true, nullable=false)
	@Size(min=8, max=60)
	@Pattern(regexp="^\\w{8,}")
	private String username;
	
	@Size(min=8)
	@Pattern(regexp="^\\S+$")
	@JsonIgnore
	private String password;
	
	@ValidEmail()
	private String email;

	@Size(min = 2, max = 254)
	private String name;
	
	@Size(min = 2, max = 254)
	private String surname;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date created;
	
    @OneToOne(mappedBy="user", cascade=CascadeType.ALL)
    @JsonIgnore
	private Authorities authorities;
    
	@Pattern(regexp="^[0-9\\,.]*$")
	@Size(min = 3, max = 12)
	private String phone;

	@Size(min = 2, max = 254)
	private String address1;
	
	private String address2;
	
	@Size(min = 4, max = 10)
	private String postcode;
	
	private boolean iscourier;
	private boolean enabled; 

	
	
	/**
	 * @return
	 * 
	 * Getters and Setters
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public java.util.Date getCreated() {
		return created;
	}

	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Authorities getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Authorities authorities) {
		this.authorities = authorities;
	}
	
//// Step 2

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public boolean isIscourier() {
		return iscourier;
	}

	public void setIscourier(boolean iscourier) {
		this.iscourier = iscourier;
	}	
	


	/**
	 * Constructors
	 */
	public User() {
		
	}

   public User(String username, String password, String email, String name,
		String surname, Date created, Authorities authorities,
		String phone, String address1, String address2, String postcode,
		boolean iscourier, boolean enabled) {
	this.username = username;
	this.password = password;
	this.email = email;
	this.name = name;
	this.surname = surname;
	this.created = created;
	this.authorities = authorities;
	this.phone = phone;
	this.address1 = address1;
	this.address2 = address2;
	this.postcode = postcode;
	this.iscourier = iscourier;
	this.enabled = enabled;
   }
  
  
  @Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
	result = prime * result + ((address2 == null) ? 0 : address2.hashCode());
	result = prime * result
			+ ((authorities == null) ? 0 : authorities.hashCode());
	result = prime * result + ((created == null) ? 0 : created.hashCode());
	result = prime * result + ((email == null) ? 0 : email.hashCode());
	result = prime * result + (enabled ? 1231 : 1237);
	result = prime * result + (iscourier ? 1231 : 1237);
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	result = prime * result + ((password == null) ? 0 : password.hashCode());
	result = prime * result + ((phone == null) ? 0 : phone.hashCode());
	result = prime * result + ((postcode == null) ? 0 : postcode.hashCode());
	result = prime * result + ((surname == null) ? 0 : surname.hashCode());
	result = prime * result + ((username == null) ? 0 : username.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	User other = (User) obj;
	if (address1 == null) {
		if (other.address1 != null)
			return false;
	} else if (!address1.equals(other.address1))
		return false;
	if (address2 == null) {
		if (other.address2 != null)
			return false;
	} else if (!address2.equals(other.address2))
		return false;
	if (authorities == null) {
		if (other.authorities != null)
			return false;
	} else if (!authorities.equals(other.authorities))
		return false;
	if (created == null) {
		if (other.created != null)
			return false;
	} else if (!created.equals(other.created))
		return false;
	if (email == null) {
		if (other.email != null)
			return false;
	} else if (!email.equals(other.email))
		return false;
	if (enabled != other.enabled)
		return false;
	if (iscourier != other.iscourier)
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	if (password == null) {
		if (other.password != null)
			return false;
	} else if (!password.equals(other.password))
		return false;
	if (phone == null) {
		if (other.phone != null)
			return false;
	} else if (!phone.equals(other.phone))
		return false;
	if (postcode == null) {
		if (other.postcode != null)
			return false;
	} else if (!postcode.equals(other.postcode))
		return false;
	if (surname == null) {
		if (other.surname != null)
			return false;
	} else if (!surname.equals(other.surname))
		return false;
	if (username == null) {
		if (other.username != null)
			return false;
	} else if (!username.equals(other.username))
		return false;
	return true;
}

/* (non-Javadoc)
   * @see java.lang.Object#toString()
   * 
   * toString
   */
  @Override
  public String toString() {
	return "User [username=" + username + ", password=" + password + ", email="
			+ email + ", name=" + name + ", surname=" + surname + ", created="
			+ created + ", authorities=" + authorities
			+ ", phone=" + phone + ", address1=" + address1 + ", address2="
			+ address2 + ", postcode=" + postcode + ", iscourier=" + iscourier
			+ ", enabled=" + enabled + "]";
  }

}
