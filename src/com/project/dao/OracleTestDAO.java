package com.project.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author raitis
 * 
 * Hibernate functions for Mytest class
 */
@Repository 
@Transactional
@Component("mytestDao")
public class OracleTestDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public void saveOrUpdate(OracleTest object) {
	    session().saveOrUpdate(object);	
	}	
	
	@Transactional
	public void save(OracleTest object) {
		object.setId("2");
		object.setName("save");
	    session().save(object);	
	}

	@SuppressWarnings("unchecked")
	public List<OracleTest> getAllOracleTestRecords() {
		return session().createQuery("from OracleTest").list();	
	}

	
}
