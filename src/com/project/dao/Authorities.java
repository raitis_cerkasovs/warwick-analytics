package com.project.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * @author raitis
 * 
 * Class Authorities, hold a row of authorities table
 * 
 */
@Entity
@Table(name="authorities")
public class Authorities implements Serializable  {
	
	private static final long serialVersionUID = 5400871506290323643L;

	@Id
	@Column(name="username", unique=true, nullable=false)
	private String username;
	   
	private String authority;	
	private String text;
	
	@OneToOne
    @PrimaryKeyJoinColumn
	private User user;
	
	
	/**
	 * Getters and setters
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	/**
	 * @param username
	 * @param authority
	 * @param text
	 * @param user
	 * 
	 * Constructors
	 */
	public Authorities(String username, String authority, String text, User user) {
		super();
		this.username = username;
		this.authority = authority;
		this.text = text;
		this.user = user;
	}

	public Authorities() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Authorities other = (Authorities) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * toString
	 */
	@Override
	public String toString() {
		return "Authorities [username=" + username + ", authority=" + authority
				+ ", text=" + text + ", user=" + user + "]";
	}

	
}