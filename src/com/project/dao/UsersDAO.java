package com.project.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author raitis
 * 
 * Hibernate functions for User class
 */
@Repository 
@Transactional
@Component("usersDao")
public class UsersDAO {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public void save(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));		
	    session().save(user);
	}
	    
	@Transactional
		public void saveOrUpdate(User user) {
		    session().saveOrUpdate(user);	    
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {	
		return session().createQuery("from User").list();		 
	}
	
	/*
	* return user according on passed authority type and which is working
	*/
	@SuppressWarnings("unchecked")
	public List<User> getUsers(String role) {
		Criteria crit = session().createCriteria(User.class);
		return crit.list();
	}


	public User getUser(String username) {
		Criteria crit = session().createCriteria(User.class);
		crit.add(Restrictions.idEq(username));
		return (User)crit.uniqueResult();
		
	}
}
